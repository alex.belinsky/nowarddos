from functools import lru_cache
from typing import List

from pydantic import BaseSettings

_DEFAULT_SITES_HOSTS = [
    "https://gitlab.com/jacobean_jerboa/sample/-/raw/main/sample"
]
_DEFAULT_PROXIES_HOSTS = [
    "https://raw.githubusercontent.com/opengs/uashieldtargets/v2/proxy.json"
]

_DEFAULT_TARGETS = [
    "https://www.kp.ru/",
    "https://ria.ru/",
    "https://lenta.ru/",
    "https://www.mk.ru/",
    "https://rg.ru/",
    "https://www.gazeta.ru/",
    "https://aif.ru/",
    "https://tass.ru/",
    "https://sber.ru/",
    "https://3dsec.sberbank.ru/",
    "https://cc-host02.sbercontact.sberbank.ru/agentdesktop/",
    "https://developer.sberbank.ru",
    "https://idppsi.sberbank.ru",
    "https://kurs-mobile.sberbank.ru",
    "https://meetup.sberbank.ru",
    "https://office.sberbank.ru",
    "https://hr-ift.sberbank.ru/",
    "https://userid.sber.ru/",
    "https://sberchat.sberbank.ru/",
    "https://web1.online.sberbank.ru",
    "https://webquik.sberbank.ru",
    "https://sberprime.sber.ru/",
    "https://platformv.sber.ru/",
    "https://friend.sber.ru/",
    "https://beta.ai.sber.ru/",
    "http://sbermail-cloud.sber.ru/",
    "https://pking.sberbank.ru/",
    "https://sberapi.sbercontact.sberbank.ru/agentdesktop/",
    "https://sber247.sbercontact.sberbank.ru/",
    "https://sbercontact.sberbank.ru/",
    "https://pmcloud.sberbank.ru/",
    "https://online.sberbank.ru/",
    "https://apps.sberbank.ru/"
]


class Settings(BaseSettings):
    MAX_THREADS: int = 500
    SITES_HOSTS: List[str] = _DEFAULT_SITES_HOSTS
    PROXIES_HOSTS: List[str] = _DEFAULT_PROXIES_HOSTS
    DEFAULT_TARGETS: List[str] = _DEFAULT_TARGETS
    MAX_REQUESTS_TO_SITE: int = 2000
    TARGET_UPDATE_RATE: int = 120
    READ_TIMEOUT: int = 10
    BROWSER: dict = {"browser": "firefox", "platform": "android", "mobile": True}


@lru_cache()
def get_settings():
    return Settings()
